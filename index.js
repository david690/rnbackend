import { initializeApp } from "firebase/app";
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from "firebase/auth";
import { getAuth } from 'firebase/auth';
import express from "express";
import { getFirestore, getDocs, collection, addDoc } from 'firebase/firestore/lite';

const firebaseConfig = {
	apiKey: "AIzaSyAxX1LJ25VpV68moq37VWj9SP8Oe1hkog8",
	authDomain: "reactnativechatapp-463a0.firebaseapp.com",
	projectId: "reactnativechatapp-463a0",
	storageBucket: "reactnativechatapp-463a0.appspot.com",
	messagingSenderId: "906519920979",
	appId: "1:906519920979:web:7fcf594bbe94b4511f61a6"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth();
const host = '0.0.0.0';
const port = 5000;
const exp = express();

exp.use(express.json());

exp.get("/", (req, res) => {
	return res.status(200).json({ "message": "Servidor funcionando correctamente." });
});

exp.post("/login", (req, res) => {
	signInWithEmailAndPassword(auth, req.body.email, req.body.password)
		.then(() => res.json({ "message": "logged" }))
		.catch((error) => {
			res.json({ "message": "error", "errorMessage": error });
		});
});

exp.post("/signup", (req, res) => {
	createUserWithEmailAndPassword(auth, req.body.email, req.body.password)
		.then(() => res.status(200).json({ "message": "created" }))
		.catch((error) => res.status(500).json({ "message": "error", "errorMessage": error }));
});

exp.get("/chats", async (req, res) => {
	try {
		const col = collection(db, "chats");
		const chatList = await getDocs(col);
		const results = chatList.docs.map(doc => doc.data());
		return res.status(200).json(results);
	} catch (error) {
		return (error) => res.status(500).json({ "message": "error", "errorMessage": error });
	}
});

exp.post("/chats", async (req, res) => {
	try {
		console.log(req.body);
		const docRef = await addDoc(collection(db, "chats"), req.body);
		return res.status(200).json({ "message": "Mensaje insertado correctamente.", "id": docRef.id });
	} catch (error) {
		return res.status(500).json({ "message": "error", "errorMessage": error });
	}
});

// exp.set('port', process.env.PORT || port);
exp.listen(process.env.PORT || port, host, () => {
	console.log(`Server listening on port http://${host}:${port}`);
});


// const appSocket = express();
// // const server = http.createServer(appSocket);
// const io = new Server();
// io.attach(serverInstance);
// io.on("connection", socket => {
// 	console.log("nuevo")
// });
// // server.listen(3000, () => console.log(`Server running at port ${3000}`));

// const appSocket = express();
// const server = http.createServer(appSocket);
// const io = new Server(server);

// appSocket.get('/', (req, res) => {
//   res.send('<h1>Hello world</h1>');
// });

// io.on('connection', (socket) => {
// 	console.log('a user connected');
//   });

// server.listen(3000, () => {
//   console.log('listening on *:3000');
// });